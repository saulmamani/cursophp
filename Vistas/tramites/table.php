<?php 
    include('../../rutas/route.php');

    $obj = new TramiteController();
    $tramites = $obj->listar();
?>

<p class="text-center">
    <button class="btn btn-info" onclick="window.print()">Imprimir</button>
</p>

<!-- Table -->
<table class="table">
<tr>
    <th>Id</th>
    <th>Nro</th>
    <th>Nombre</th>
    <th>Opciones</th>
</tr>

<?php while($row = $tramites->fetch_object()): ?>
<tr>
    <td> <?php echo $row->id ?> </td>
    <td> <?php echo $row->numero ?> </td>
    <td> <?php echo $row->nombre ?> </td>

    <td>
        <a href="edit.php?id=<?php echo $row->id ?>" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-edit"></span> Modificar</a>
        <a href="eliminar.php?id=<?php echo $row->id ?>" class="btn btn-sm btn-danger" onclick="return confirm('estas seguro?')"><span class="glyphicon glyphicon-remove"></span> Eliminar</a>
        <a href="show.php?id=<?php echo $row->id ?>" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-eye-open"></span> Ver</a>
    </td>
</tr>
<?php endwhile; ?>

</table>