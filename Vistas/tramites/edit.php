<?php
    $id = $_GET['id'];

    include('../../rutas/route.php');
    $obj = new TramiteController();
    $reg = $obj->mostrar($id)->fetch_object();
    //var_dump($reg->nombre);
?>

<?php include('../templates/app.php') ?>

<div class="container">

    <div class="row">
        <div class="col-sm-2"></div>

        <div class="col-sm-8">
            <h1>Editar Tramite</h1>
            <hr>
            <form id="frmDatos" role="form" action="update.php" method="POST">
                    
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                    
                <div class="form-group">
                    <label for="numero">Numero: </label>
                    <input type="text" value=" <?php echo $reg->numero ?> " class="form-control" id="numero" name="numero" placeholder="" required>
                </div>

                <div class="form-group">
                    <label for="nombre">Nombre: </label>
                    <input type="text" value=" <?php echo $reg->nombre ?> " class="form-control" id="nombre" name="nombre" placeholder="" required>
                </div>

                <button id="btnGuardar" class="btn btn-default">Guardar</button>
            </form>

            <div id="result"></div>
            <div id="grilla"></div>

        </div>

        <div class="col-sm-2"></div>
    </div>

</div>

<?php include('../templates/footer.php') ?>

