<?php
    include('../../rutas/route.php');

    $id = $_GET['id'];

    $obj = new TramiteController();
    if($obj->eliminar($id)):
?>

<?php include('../templates/app.php') ?>

<div class="container">

    <div class="alter alert-success">Registro eliminado Correctamente!!!</div>
    
    <?php else: ?>
    
    <div class="alter alter-danger">Ha ocurrido un error, vuelva a intentarlo</div>
    
    <?php endif; ?>
    
    <button class="btn btn-lg btn-primary" onclick="history.back(-1)">Volver...</button>

</div>

<?php include('../templates/footer.php') ?>
