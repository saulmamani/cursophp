<?php include('../templates/app.php') ?>

<div class="container">
    <h1>Lista de tramites</h1>
    <hr>
    <button class="btn btn-default">Buscar</button>
    <button class="btn btn-primary">Buscar</button>
    <button class="btn btn-success">Buscar</button>
    <button class="btn btn-danger">Buscar</button>
    <button class="btn btn-warning">Buscar</button>
    <button class="btn btn-info">Buscar</button>

<hr>

<p class="text-right">
    <a href="create.php" class="btn btn-lg btn-primary">
        <span class="glyphicon glyphicon-plus"></span>
        Nuevo Tramite
    </a>
</p>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Lista</div>

    <?php include('table.php') ?>

</div>


</div>

<?php include('../templates/footer.php') ?>