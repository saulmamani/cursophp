<?php
    include('../../rutas/route.php');

    //recogiendo variable desde la url con GET
    $id = $_GET['id'];

    $obj = new TramiteController();
    $result = $obj->mostrar($id);
    //print_r($result);
    $tramite = $result->fetch_object();
?>

<?php include('../templates/app.php') ?>

    <div class="container">
            
        <div class="row">
            <div class="col-sm-2"></div>
            
            <div class="col-sm-8">
                <h1>Detalle</h1>
                <hr>
                

                <ul class="list-group">
                    <li class="list-group-item"> <strong>Id: </strong> <?php echo $tramite->id ?> </li>
                    <li class="list-group-item"> <strong>Nro.: </strong> <?php echo $tramite->numero ?> </li>
                    <li class="list-group-item"> <strong>Nombre: </strong> <?php echo $tramite->nombre ?></li>
                </ul>

                <hr>
                <a href="./" class="btn btn-lg btn-default">Volver</a>

            </div>  

            <div class="col-sm-2"></div> 
        </div>

    </div>


<?php include('../templates/footer.php') ?>
